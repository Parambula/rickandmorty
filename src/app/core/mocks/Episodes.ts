import { Episode } from '../models/episode';
import { EpisodeResponse } from '../models/episodeResponse';

export const mockEpisode: Episode = {
  id: 28,
  name: 'The Ricklantis Mixup',
  air_date: 'September 10, 2017',
  episode: 'S03E07',
  characters: [
    'https://rickandmortyapi.com/api/character/1',
    'https://rickandmortyapi.com/api/character/2',
  ],
  url: 'https://rickandmortyapi.com/api/episode/28',
  created: '2017-11-10T12:56:36.618Z',
};

export const mockEpisodes: Episode[] = [
  {
    id: 10,
    name: 'Close Rick-counters of the Rick Kind',
    air_date: 'April 7, 2014',
    episode: 'S01E10',
    characters: [
      'https://rickandmortyapi.com/api/character/1',
      'https://rickandmortyapi.com/api/character/2',
    ],
    url: 'https://rickandmortyapi.com/api/episode/10',
    created: '2017-11-10T12:56:34.747Z',
  },
  {
    id: 28,
    name: 'The Ricklantis Mixup',
    air_date: 'September 10, 2017',
    episode: 'S03E07',
    characters: [
      'https://rickandmortyapi.com/api/character/1',
      'https://rickandmortyapi.com/api/character/2',
    ],
    url: 'https://rickandmortyapi.com/api/episode/28',
    created: '2017-11-10T12:56:36.618Z',
  },
];

export const mockEpisodeFirstPage: EpisodeResponse = {
  info: {
    count: 671,
    pages: 34,
    next: 'https://rickandmortyapi.com/api/episode/?page=2',
  },
  results: [],
};

export const mockEpisodeLastPage: EpisodeResponse = {
  info: {
    count: 671,
    pages: 34,
    prev: 'https://rickandmortyapi.com/api/episode/?page=33',
  },
  results: [],
};
