import { Episode } from './episode';
import { PageInfo } from './pageInfo';
export interface EpisodeResponse {
  info: PageInfo;
  results: Episode[];
}