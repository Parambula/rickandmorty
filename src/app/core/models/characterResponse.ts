import { Character } from './character';
import { PageInfo } from './pageInfo';
export interface CharacterResponse {
  info: PageInfo;
  results: Character[];
}
