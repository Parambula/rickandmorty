import { CharacterService } from './character.service';
import { of } from 'rxjs/internal/observable/of';
import {
  mockCharacter,
  mockCharacters,
  mockCharactersFirstPage,
  mockCharactersLastPage
} from '../mocks/Charactes';

describe('Character Service', () => {
  let httpClientSpy: { get: jasmine.Spy };
  let service: CharacterService;

  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get']);
    service = new CharacterService(httpClientSpy as any);
  });

  it('Character Service should be instanced correctly', () => {
    expect(service).toBeTruthy();
  });

  it('should return a character equals to the ID provided', (done) => {
    const ID = 2;
    httpClientSpy.get.and.returnValue(of(mockCharacter));

    service.getCharacterByID(ID).then((character) => {
      expect(character.id).toEqual(ID);
    });

    expect(httpClientSpy.get.calls.count()).toBe(1, 'one call');
    done();
  });

  it('should return an array of character equals to the IDs provided', (done) => {
    const IDs = [1, 2, 3];
    const items = IDs.length;
    httpClientSpy.get.and.returnValue(of(mockCharacters));

    service.getMultipleCharactersById(IDs).then((characters) => {
      for (let i = 0; i < items; i++) {
        expect(characters[i].id).toEqual(IDs[i]);
      }
    });
    done();
  });

  it('should return a new url with the next page depending on the url sended', (done) => {
    const page = 1;
    const url = `https://rickandmortyapi.com/api/character/?page=${page}`;
    const expectedUrl = `https://rickandmortyapi.com/api/character/?page=${page+1}`;
    httpClientSpy.get.and.returnValue(of(mockCharactersFirstPage));

    service.getCharacterByURL(url).then( (response) => {
      expect(response.info.next).toBe(expectedUrl);
    });
    done();
  });
  
  it('should return an undefine property on info.prev if the page sended is the first one', (done) => {
    const page = 1;
    const url = `https://rickandmortyapi.com/api/character/?page=${page}`;
    
    httpClientSpy.get.and.returnValue(of(mockCharactersFirstPage));

    service.getCharacterByURL(url).then( (response) => {
      expect(response.info.prev).toBeUndefined();
    });
    done();
  });
  
  it('should return an undefine property on info.next if the page sended is the last one', (done) => {
    const page = 1;
    const url = `https://rickandmortyapi.com/api/character/?page=${page}`;
    
    httpClientSpy.get.and.returnValue(of(mockCharactersLastPage));

    service.getCharacterByURL(url).then( (response) => {
      expect(response.info.next).toBeUndefined();
    });
    done();
  });
});
