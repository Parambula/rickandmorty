import { Injectable } from '@angular/core';
import { Episode } from '../models/episode';
import { HttpClient, HttpParams } from '@angular/common/http';
import { urlAPI, episodeEndPoint } from '../consts/api';
import { EpisodeResponse } from '../models/episodeResponse';

@Injectable({
  providedIn: 'root',
})
export class EpisodeService {
  constructor(private httpClient: HttpClient) {}

  getEpisodeByID(ID: number): Promise<Episode> {
    return this.httpClient
      .get<Episode>(`${urlAPI}${episodeEndPoint}${ID}`)
      .toPromise();
  }

  getMultipleEpisodesById(IDs: number[]): Promise<Episode[]> {
    return this.httpClient
      .get<Episode[]>(`${urlAPI}${episodeEndPoint}${IDs}`)
      .toPromise();
  }

  getEpisodesPerPage(page: number): Promise<EpisodeResponse> {
    const params = new HttpParams().set('page', page.toString());
    return this.httpClient
      .get<EpisodeResponse>(`${urlAPI}${episodeEndPoint}`, { params })
      .toPromise();
  }

  getEpisodeByURL(url: string): Promise<EpisodeResponse> {
    return this.httpClient.get<EpisodeResponse>(url).toPromise();
  }
}
