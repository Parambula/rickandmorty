import { Injectable } from '@angular/core';
import { Character } from '../models/character';
import { CharacterResponse } from '../models/characterResponse';
import { HttpClient, HttpParams } from '@angular/common/http';
import { urlAPI, characterEndPoint } from '../consts/api';

@Injectable({
  providedIn: 'root',
})
export class CharacterService {
  constructor(private httpClient: HttpClient) {}

  getCharacterByID(ID: number): Promise<Character> {
    return this.httpClient
      .get<Character>(`${urlAPI}${characterEndPoint}${ID}`)
      .toPromise();
  }

  getMultipleCharactersById(IDs: number[]): Promise<Character[]> {
    return this.httpClient
      .get<Character[]>(`${urlAPI}${characterEndPoint}${IDs}`)
      .toPromise();
  }

  getCharactersPerPage(page: number): Promise<CharacterResponse> {
    const params = new HttpParams().set('page', page.toString());
    return this.httpClient
      .get<CharacterResponse>(`${urlAPI}${characterEndPoint}`, { params })
      .toPromise();
  }

  getCharacterByURL(url: string): Promise<CharacterResponse> {
    return this.httpClient.get<CharacterResponse>(url).toPromise();
  }
}
