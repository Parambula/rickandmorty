import { EpisodeService } from './episode.service';
import { of } from 'rxjs/internal/observable/of';
import {
  mockEpisode,
  mockEpisodes,
  mockEpisodeFirstPage,
  mockEpisodeLastPage
} from '../mocks/Episodes';

describe('Episode Service', () => {
  let httpClientSpy: { get: jasmine.Spy };
  let service: EpisodeService;

  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get']);
    service = new EpisodeService(httpClientSpy as any);
  });

  it('Episode Service should be instanced correctly', () => {
    expect(service).toBeTruthy();
  });

  it('should return a episode equals to the ID provided', (done) => {
    const ID = 28;
    httpClientSpy.get.and.returnValue(of(mockEpisode));

    service.getEpisodeByID(ID).then((episode) => {
      expect(episode.id).toEqual(ID);
    });

    expect(httpClientSpy.get.calls.count()).toBe(1, 'one call');
    done();
  });

  it('should return an array of episodes equals to the IDs provided', (done) => {
    const IDs = [10, 28];
    const items = IDs.length;
    httpClientSpy.get.and.returnValue(of(mockEpisodes));

    service.getMultipleEpisodesById(IDs).then((episodes) => {
      for (let i = 0; i < items; i++) {
        expect(episodes[i].id).toEqual(IDs[i]);
      }
    });
    done();
  });

  it('should return a new url with the next page depending on the url sended', (done) => {
    const page = 1;
    const expectedUrl = `https://rickandmortyapi.com/api/episode/?page=${
      page + 1
    }`;
    httpClientSpy.get.and.returnValue(of(mockEpisodeFirstPage));

    service.getEpisodesPerPage(page).then((response) => {
      expect(response.info.next).toBe(expectedUrl);
    });
    done();
  });

  it('should return an undefine property on info.prev if the page sended is the first one', (done) => {
    const page = 1;
    const url = `https://rickandmortyapi.com/api/episode/?page=${page}`;

    httpClientSpy.get.and.returnValue(of(mockEpisodeFirstPage));

    service.getEpisodeByURL(url).then((response) => {
      expect(response.info.prev).toBeUndefined();
    });
    done();
  });

  it('should return an undefine property on info.next if the page sended is the last one', (done) => {
    const page = 1;
    const url = `https://rickandmortyapi.com/api/episode/?page=${page}`;

    httpClientSpy.get.and.returnValue(of(mockEpisodeLastPage));

    service.getEpisodeByURL(url).then((response) => {
      expect(response.info.next).toBeUndefined();
    });
    done();
  });
});
