export const urlAPI = "https://rickandmortyapi.com/api/";
export const characterEndPoint = "character/";
export const episodeEndPoint = "episode/"