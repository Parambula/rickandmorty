import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: () =>
      import('./components/character/character.module').then(
        (m) => m.CharacterModule
      ),
    data: {},
  },
  {
    path: 'episode',
    loadChildren: () =>
      import('./components/episode/episode.module').then(
        (m) => m.EpisodeModule
      ),
    data: {},
  },
  {
    path: 'location',
    loadChildren: () =>
      import('./components/location/location.module').then(
        (m) => m.LocationModule
      ),
    data: {},
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { onSameUrlNavigation: 'reload' })],
  exports: [RouterModule],
})
export class AppRoutingModule {}
