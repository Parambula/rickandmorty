import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EPISODE_DETAIL } from '../../core/consts/routes';
import { EpisodeDetailComponent } from './episode-detail/episode-detail.component'

const routes: Routes = [
  {
    path: EPISODE_DETAIL,
    component: EpisodeDetailComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EpisodeRoutingModule {}
