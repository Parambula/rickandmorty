import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { EpisodeService } from '../../../core/services/episode.service';
import { Episode } from 'src/app/core/models/episode';
@Component({
  selector: 'app-episode-detail',
  templateUrl: './episode-detail.component.html',
})
export class EpisodeDetailComponent implements OnInit {
  constructor(private route: ActivatedRoute, private service: EpisodeService) {}

  episode!: Episode;

  ngOnInit(): void {
    let stringId = this.route.snapshot.paramMap.get('id');
    if (stringId) {
      let EpisodeId: number = +stringId;
      if (EpisodeId >= 0) {
        this.service
          .getEpisodeByID(EpisodeId)
          .then((episode) => (this.episode = episode));
      }
    }
  }
}
