import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Character } from 'src/app/core/models/character';
import { Episode } from 'src/app/core/models/episode';
import { CharacterService } from '../../../core/services/character.service';
import { EpisodeService } from '../../../core/services/episode.service';

@Component({
  selector: 'app-character-detail',
  templateUrl: './character-detail.component.html',
  styleUrls: ['./character-detail.component.css'],
})
export class CharacterDetailComponent implements OnInit {
  character!: Character;
  episodes!: Episode[];
  originLocationId: number = 0;
  actualLocationId: number = 0;

  constructor(
    private serviceCharacter: CharacterService,
    private serviceEpisode: EpisodeService,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    let stringId = this.route.snapshot.paramMap.get('id');
    if (stringId) {
      let CharacterId: number = +stringId;
      if (CharacterId >= 0) {
        this.serviceCharacter
          .getCharacterByID(CharacterId)
          .then((character) => {
            this.character = character;
            this.setIDsLocation();
            let IDsOfEpisodes = this.parseIDsfromUrls(this.character.episode);
            this.getEpisodes(IDsOfEpisodes);
          });
      }
    }
  }

  setIDsLocation(): void {
    this.originLocationId = this.parseIDfromUrl(this.character.origin.url);
    this.actualLocationId = this.parseIDfromUrl(this.character.location.url);
  }

  getEpisodes(IDsOfEpisodes: number[]): void {
    this.serviceEpisode
      .getMultipleEpisodesById(IDsOfEpisodes)
      .then((episodes) => {
        this.episodes = episodes;
      });
  }

  parseIDsfromUrls(Urls: string[]) {
    let ids: number[] = [];

    Urls.forEach((url) => {
      let id = +url.substring(url.lastIndexOf('/') + 1);
      ids.push(id);
    });

    return ids;
  }

  parseIDfromUrl(Url: string) {
    let id = +Url.substring(Url.lastIndexOf('/') + 1);
    return id;
  }
}
