import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CharacterListComponent } from './character-list/character-list.component';
import { CharacterDetailComponent } from './character-detail/character-detail.component';
import { CHARACTER_LIST, CHARACTER_DETAIL } from '../../core/consts/routes';

const routes: Routes = [
  {
    path: CHARACTER_LIST,
    component: CharacterListComponent,
  },
  {
    path: CHARACTER_DETAIL,
    component: CharacterDetailComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CharacterRoutingModule {}
