import { Component, OnInit } from '@angular/core';
import { CharacterService } from '../../../core/services/character.service';
import { Character } from '../../../core/models/character';
import { PageInfo } from 'src/app/core/models/pageInfo';

@Component({
  selector: 'app-character-list',
  templateUrl: './character-list.component.html',
  styleUrls: ['./character-list.component.css'],
})
export class CharacterListComponent implements OnInit {
  characters: Character[] | undefined;
  pages: PageInfo | undefined;
  actualPage: number = 1;
  lastPage: number = 0;

  constructor(private service: CharacterService) {}

  ngOnInit(): void {
    this.service.getCharactersPerPage(this.actualPage).then((charactersResponse) => {
      this.characters = charactersResponse.results;
      this.pages = charactersResponse.info;
      this.lastPage = this.pages.pages;
    });
  }

  async getNewPage(moveTo: boolean): Promise<void> {
    let prevPage = this.pages?.prev!;
    let nextPage = this.pages?.next!;
    moveTo
      ? await this.service.getCharacterByURL(prevPage).then((characterResponse) => {
          this.pages = characterResponse.info;
          this.characters = characterResponse.results;
          this.actualPage != 1 ? this.actualPage -- : this.actualPage = 1;
        })
      : await this.service.getCharacterByURL(nextPage).then((characterResponse) => {
          this.pages = characterResponse.info;
          this.characters = characterResponse.results;
          this.actualPage < this.lastPage ? this.actualPage ++ : this.actualPage = this.lastPage;
        });
  }
}
