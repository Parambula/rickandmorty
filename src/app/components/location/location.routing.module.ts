import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LOCATION_DETAIL } from '../../core/consts/routes';
import { LocationDetailComponent } from './location-detail/location-detail.component';

const routes: Routes = [
  {
    path: LOCATION_DETAIL,
    component: LocationDetailComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LocationRoutingModule {}
